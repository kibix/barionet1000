#
# Copyright (C) 2015 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/BAR1KOEM-A1
	NAME:=BAR1KOEM-A1
	KCONFIG:= \
		CONFIG_IIO=y \
		CONFIG_IIO_BUFFER=y \
		CONFIG_IIO_BUFFER_CB=y \
		CONFIG_IIO_KFIFO_BUF=y \
		CONFIG_IIO_TRIGGER=y \
		CONFIG_MCP320X=y
	PACKAGES:= \
		kmod-usb-core kmod-usb-ohci kmod-usb2 \
		kmod-usb-serial kmod-usb-serial-ftdi kmod-usb-serial-pl2303 \
		kmod-usb-storage kmod-usb-storage-extras kmod-scsi-core \
		kmod-nls-utf8 kmod-nls-cp437 kmod-nls-cp1250 kmod-nls-cp1251 kmod-nls-iso8859-1 \
		kmod-leds-gpio kmod-ledtrig-netdev \
		kmod-fs-vfat kmod-fs-msdos block-mount libusb usbutils \
		kmod-i2c-core kmod-i2c-ralink \
		kmod-w1 kmod-w1-master-ds2482 kmod-w1-slave-therm \
		kmod-barionet-gpio barionet-www \
		opkg logger
endef

define Profile/BAR1KOEM-A1/Description
	Support for Barionet 1000 OEM (rev.A1) device
endef
$(eval $(call Profile,BAR1KOEM-A1))

