#!/bin/ash

# led bicolor enable/disable
# 2016-01-25 : replace gpio with gpio-led devices FOR QUINO!!!!!
# 2016-03-22 : gpio-leds label changed from quino to qino
# 2016-04-18 : gpio-leds replaced by custom driver, by toggle pull_up

file_name=$(basename $0)
echo "0.05" > /tmp/ver_${file_name}

# initialize Gpios

usage(){

        echo -e "Usage: $(basename $0) on/off r{11-18}  "
	echo -e "Led name is range r{11-18}, example"
	echo -e "${filename} on r11 "
	echo -e "${filename} off r11 "
}

# toogle led status
if [ "$1" = "on" ]; then
        let led_sw=0
elif [ "$1" = "off" ]; then
        let led_sw=1
else
        usage
        exit 1
fi

# choose correct led

case "$2" in

        r11 )
        echo -n "${led_sw}" > /sys/class/gpio/gpio488/value
        ;;

        r12 )
        echo -n "${led_sw}" > /sys/class/gpio/gpio489/value
        ;;

        r13 )
        echo -n "${led_sw}" > /sys/class/gpio/gpio490/value
        ;;

        r14 )
        echo -n "${led_sw}" > /sys/class/gpio/gpio491/value
        ;;

        r15 )
        echo -n "${led_sw}" > /sys/class/gpio/gpio492/value
        ;;

        r16 )
        echo -n "${led_sw}" > /sys/class/gpio/gpio493/value
        ;;

        r17 )
        echo -n "${led_sw}" > /sys/class/gpio/gpio494/value
        ;;

        r18 )
        echo -n "${led_sw}" > /sys/class/gpio/gpio495/value
        ;;

        * )
        usage
        exit 1
        ;;

esac

