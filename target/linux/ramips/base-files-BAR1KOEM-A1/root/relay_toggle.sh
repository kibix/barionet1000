#!/bin/ash


# Enable /disable relay with spi bursts....
# v0.01a 2016-02-03 : use bit by bit, instead of burst
# v0.02 2016-02-04 : assign gpio69 as enable chip and gpio34 as master enable
# v0.03 2016-02-13 : use flags to retain relay state
# v0.04 2016-04-15 : drop spi driver and use sysfs driver


my_name=`basename $0`
echo "0.04" > /tmp/ver_${my_name}



usage(){
	echo -e "Usage: $(basename $0) on/off [1,2]  "
}




# toogle gpio
if [ "$1" = "on" ]; then
	pio_sw="1"
elif [ "$1" = "off" ]; then
	pio_sw="0"
else
	usage
	exit 1
fi



#echo -e "Pin: $2 , Value: $2 \n";
#select relay gpio and toggle it

case "$2" in

	1 )
	
	echo -ne "$pio_sw" > /sys/devices/virtual/gpio/gpio501/value
	
	;;
	2 )
	
	echo -ne "$pio_sw" > /sys/devices/virtual/gpio/gpio500/value
	;;
	* )

	usage
	exit 1
	;;

esac
