#!/bin/ash


file_name=$(basename $0)
echo "0.04" > /tmp/ver_${file_name}

usage(){

        echo -e "usage: $(basename $0) p{0-11} v{0/1}  "
	echo -e "Pin name is range p{0-11}, example"
	echo -e "${filename} -p0 -v1 -> I1 On"
}

# toogle port status

# toogle led status
if [ "$2" = "-v1" ]; then
        let pin_sw=1
elif [ "$2" = "-v0" ]; then
        let pin_sw=0
else
        usage
        exit 1
fi

#choose correct pin


case "$1" in

	# Digital Gpios
        -p0 )
        echo out > /sys/devices/virtual/gpio/gpio504/direction	
	echo -n "${pin_sw}" > /sys/devices/virtual/gpio/gpio504/value
        ;;

        -p1 )
        echo out > /sys/devices/virtual/gpio/gpio504/direction	
        echo -n "${pin_sw}" > /sys/devices/virtual/gpio/gpio505/value
        ;;

        -p2 )
        echo out > /sys/devices/virtual/gpio/gpio504/direction	
        echo -n "${pin_sw}" > /sys/devices/virtual/gpio/gpio506/value
        ;;

        -p3 )
        echo out > /sys/devices/virtual/gpio/gpio504/direction	
        echo -n "${pin_sw}" > /sys/devices/virtual/gpio/gpio507/value
        ;;

        -p4 )
        echo out > /sys/devices/virtual/gpio/gpio504/direction	
        echo -n "${pin_sw}" > /sys/devices/virtual/gpio/gpio508/value 
        ;;

        -p5 )
        echo out > /sys/devices/virtual/gpio/gpio504/direction	
        echo -n "${pin_sw}" > /sys/devices/virtual/gpio/gpio509/value
        ;;

        -p6 )
        echo out > /sys/devices/virtual/gpio/gpio504/direction	
        echo -n "${pin_sw}" > /sys/devices/virtual/gpio/gpio510/value 
        ;;

        -p7 )
        echo out > /sys/devices/virtual/gpio/gpio504/direction	
        echo -n "${pin_sw}" > /sys/devices/virtual/gpio/gpio511/value
        ;;

	# Digital Mosfets


        -p8 )
#        echo out > /sys/devices/virtual/gpio/gpio504/direction	
        echo -n "${pin_sw}" > /sys/devices/virtual/gpio/gpio496/value 
        ;;

        -p9 )
#        echo out > /sys/devices/virtual/gpio/gpio504/direction	
        echo -n "${pin_sw}" > /sys/devices/virtual/gpio/gpio497/value
        ;;

        -p10 )
#        echo out > /sys/devices/virtual/gpio/gpio504/direction	
        echo -n "${pin_sw}" > /sys/devices/virtual/gpio/gpio498/value 
        ;;

        -p11 )
#        echo out > /sys/devices/virtual/gpio/gpio504/direction	
        echo -n "${pin_sw}" > /sys/devices/virtual/gpio/gpio499/value
	;;

	# serial port
        -p15 )
#        echo out > /sys/devices/virtual/gpio/gpio504/direction	
        echo -n "${pin_sw}" > /sys/devices/virtual/gpio/gpio503/value
	;;
	
        * )
        usage
        exit 1
        ;;

esac




