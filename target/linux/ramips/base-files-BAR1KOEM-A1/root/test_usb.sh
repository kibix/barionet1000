#!/bin/ash

# test usb disk 

# test two usb disks

# 0.03 2016-10-18: Change string search
# 0.04 2016-10-18: Fixed number of mountpoint units to test
# 0.05 2016-10-31: Using enable / disable routines for USB bus
# 0.06 2016-10-31: Using 



myname=`basename $0`
echo "0.07" > /tmp/ver_${myname}

# mount_label=`dmesg | grep -A 4 "USB Mass Storage" | sed -nr "s/\r//;s/(.*)((sd[a-z])+)(.*)/\2/p"`


#kmod_load() {
#        local modname=$1
#        [[ -z $modname ]] && return -1
#
#        local dependents=$(lsmod | grep "$modname" | sed -n -e "s/$modname[[:space:]]\+[0-9]\+[[:space:]]\+[0-9]\+[[:space:]]\+\(.*\)/\1/Igp")
#	for childmod in $(echo -n "$dependents" | sed 's/,/ /g')
#        do
#                kmod_load $childmod
#       done
#
#        return $(modprobe "$modname")
#}






let NR_MNT=2
filename="mnt_txt.$$"

parm=$1

# enable usb drives:
#echo "1-1" > /sys/bus/usb/drivers/usb/bind > /dev/null 2>&1

USB_STATUS=`uci get barionet.usb.disabled`

if [ "$USB_STATUS" == "1" ]; then

	uci set barionet.usb.disabled='0'
	uci commit barionet
	kmod_load "usb_core"
	/etc/init.d/boot boot

		
	/bin/sleep 5
fi





$( dmesg -c | grep 'Attached SCSI removable disk' > /tmp/$filename )
mount_label=`sed -nr "s/\r//;s/(.*)((sd[a-z])+)(.*)/\2/p" /tmp/$filename`

#echo "Mount Labels:"
#echo "$mount_label"

if [ "$mount_label" == "" ]; then
	echo -ne "usb;KO"
fi

let drv_cnt=0 

for i in $mount_label; 
do 
	dev=`ls /dev/$i?`
	list_dev="${dev} ${list_dev}"
	let drv_cnt=$drv_cnt+1
#	printf " %s | %s" $i $dev
done

#echo "$list_dev"

txt=`date +"%Y-%m-%d_%H"`

#exit 0

if [ $drv_cnt -lt $NR_MNT ]; then
	let drv_cnt=$NR_MNT
	list_dev="/dev/dummy $list_dev"
fi


#echo "Mounts Number: $drv_cnt"

if [ "$list_dev" != "" ] && [ $drv_cnt -gt 0 ]; then
	
	let cnt=0
	for j in $list_dev;
	do
		mkdir -p /media/usbdisk${cnt}
		mount $j /media/usbdisk${cnt}
		if [ $? != 0 ]; then
			usb_val=1
		else
			usb_val=0
		fi
		
		if [ $usb_val == "0" ]; then
			cd /media/usbdisk${cnt}
			echo ${txt} > test.txt	
			usb_str=`cat /media/usbdisk${cnt}/test.txt`	
		else
			usb_str=""
		fi
	
	if [ "$usb_str" == "$txt" ]; then
		usb_val="1"
		echo -ne "usb;OK "
	
	else
		usb_val="0"
		echo -ne "usb;KO "
	fi

	cd /
	umount $j
			
	rm -rf /media/usbdisk${cnt}
	
	let cnt=$cnt+1	

	done

#	if [ "$usb_val" == "1" ]; then
#		echo -ne "usb;OK "
#	else
#		echo -ne "usb;KO "
#	fi


#	echo "Tested drives: $cnt"
else
		echo -ne "usb;KO "
fi

# disable USB
#kmod_unload "usb_core"
#echo "1-1" > /sys/bus/usb/drivers/usb/unbind > /dev/null 2>&1
/bin/sleep 3

rm /tmp/$filename
