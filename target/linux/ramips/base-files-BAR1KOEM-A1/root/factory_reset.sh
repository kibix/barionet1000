#!/bin/ash

#2016-05-11: Moved gzip and tar operations before erasing overlay filesystem
#2016-05-30: Modified version for Qino Lite
#2016-06-20: Correction output on Socket Implementations (Qino lite Boards)
#2016-08-11: Change target name for testmode program.
#2016-11-03: Change target for Qino Barionet1k and Default IP addr

bck_file="serial_testmode-Qino"
#test_progs=`cat /root/serial_testmode.lst`



cd /tmp/

if [ -f ${bck_file}.tar.gz ]; then
	rm ${bck_file}.tar.gz
	rm /tmp/bck_files.lst	
fi

# get the backup file
# wget http://192.168.10.227:8080/${bck_file}.tar.gz 2> /tmp/update.err 
wget http://192.168.1.227:8080/${bck_file}.tar.gz 2> /tmp/update.err 

if [ "$?" != "0" ]; then
	echo -ne "res;KO"
	logger ${bck_file} not found
	cat /tmp/update.err
	exit 1
fi

if [ -f ${bck_file}.tar.gz ]; then

	# check dir available
	if [ -d /overlay/upper/ ]; then	
		
		gzip -d ${bck_file}.tar.gz
		tar -tvf ${bck_file}.tar > /tmp/test_upd.lst
		
		if [ "$?" == "0" ]; then

			mount_root
			/bin/sleep 1
			rm -r /overlay/upper/*
			sync
				
			tar -xvf ${bck_file}.tar -C /overlay/upper/. > /tmp/update.fl
	
			sync

			/bin/sleep 2
				
			echo -ne "res;OK"
			logger ${bck_file} has been deployed, rebooting...
		
			reboot -d 3 -f
		
		else
			logger ${bck_file} is empty or corrupted, aborting..	
			cat /tmp/update.err
			echo -ne "res;KO"
			exit 2
		fi	
	
	fi
	
	
else
	echo -ne "res;KO"
	logger ${bck_file} tarball missing
	exit 2
fi 

#sync
#/bin/sleep 2

#for f in $test_progs 
#do
#
#	if [ -f $f ]; then
#		#rm -rf "file: $f"
#		echo "file : $f" 
#		echo -ne $f >> /tmp/files_erased.lst
#		logger $f to be erased
#		
#	fi
#
#done


