#!/bin/ash

# Test 1-Wire Temperature sensor. 

file_name=$(basename $0)
echo "0.01" > /tmp/ver_${file_name}


sensor_data=`cat /sys/bus/w1/devices/28-*/w1_slave`

raw_val=`echo "${sensor_data}" | sed -e "2!d s/\r//;s/.*t=\b/\1/"`

if [ "$raw_val" != "" ]; then 
	echo -ne "$raw_val"
else
	echo -ne "temp;KO"
	exit 1
fi
