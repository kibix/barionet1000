#!/bin/ash

#luci wifi fct check
#2016-04-20: enable wifi if not
#2016-10-31: force restart wifi even if not connected


myname=`basename $0`
echo "0.05" > /tmp/ver_${myname}

# check wifi disabled
wifi_stat=`uci show wireless | grep disabled=\'1\'`
#wifi_stat=`uci show wireless | grep disabled=\'1\'`


iparm=$1

if [ "$wifi_stat" != "" ]; then
	#echo -ne "wlan;KO"
	uci set wireless.radio0.disabled='0'
	uci set wireless.@wifi-iface[0].hidden='0'
	SSID="Barionet$(hexdump -v -e '1/1 "%.2x"' -s $((0x07)) -n 3 /dev/mtd2)"
	uci set wireless.@wifi-iface[0].ssid=$SSID
	uci commit wireless
	/bin/sleep 1
	
	# toggle wifi module
	/etc/init.d/network restart
	#/sbin/wifi down
	#/sbin/wifi up
	/bin/sleep 6
fi	
#else
	
	if [ "$iparm" != "" ]; then
		probe_wlan=`/bin/ping ${iparm} -4 -c 5 -q`
		echo $probe_wlan > /tmp/wlan_test
		test_wlan=`cat /tmp/wlan_test  | grep "5 packets received"`
	else
		test_wlan=`/bin/ping www.google.pt -4 -c 5  | grep "5 packets received"`
	fi
	
	if [ "$test_wlan" != "" ]; then  
		echo -ne "wlan;OK"
	else
		echo -ne "wlan;KO"
	fi

#fi

