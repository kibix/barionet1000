#!/bin/ash

# 2016-04-21: added arguments for distinct interfaces

file_name=$(basename $0)
echo "0.04" > /tmp/ver_${file_name}

param=$1

if [ "$param" == "--wifi" ]; then
	mac_get=`ifconfig wlan0 | grep -o -E "([0-9A-F]{2}\:){5}([0-9A-F]{2})" | tr '[A-Z]' '[a-z]'`
else
	mac_get=`hexdump -v -e '1/1 "%.2x:"' -n 6 -s $((0x28)) /dev/mtd2 | sed -r "s/:$//g"`
fi


echo -ne ${mac_get}

