#!/bin/ash

#test eth0.2 lan interface
file_name=$(basename $0)
echo "0.03" > /tmp/ver_${file_name}

iparm=$1

if [ "$iparm" != "" ]; then
	test_lan=`/bin/ping ${iparm} -4 -c 5 -q | grep "5 packets received"`
else
	test_lan=`/bin/ping www.google.pt -4 -c 5 -q | grep "5 packets received"`
fi

if [ "$test_lan" != "" ]; then
	echo -ne "lan;OK"
else
	echo -ne "lan;KO"
fi
