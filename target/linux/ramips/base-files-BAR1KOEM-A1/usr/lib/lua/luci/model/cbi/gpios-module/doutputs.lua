local utl = require "luci.util"

m = Map("gpios", translate("GPIO Configuration"), translate("Please select 0 or 1 "))

d = m:section(TypedSection, "douts", "Digital Outputs Toggle Section ")  -- info is the section called info in cbi_file

-- Button go back to overview
m.redirect = luci.dispatcher.build_url("admin", "custom", "gpios")

a = d:option(Value, "dout1", "Digital Out 1"); a.optional=false; a.rmempty = false;
b = d:option(Value, "dout2", "Digital Out 2"); b.optional=false; b.rmempty = false;
c = d:option(Value, "dout3", "Digital Out 3"); c.optional=false; c.rmempty = false;
e = d:option(Value, "dout4", "Digital Out 4"); e.optional=false; e.rmempty = false;

function a:validate(value)
	local out1
	--return value:match("[0-1]")
	out1 = value:match("[0-1]")	
	
	if out1 == nil then
		return nil
	else
		local cmd = "/bin/echo " .. out1 .. " > /sys/devices/virtual/gpio/gpio496/value" 
		luci.util.exec(cmd)
		return out1
	end
end

function b:validate(value)
	local out2
	--return value:match("[0-1]")
	out2 = value:match("[0-1]")
	
	if out2 == nil then
		return nil
	else
		local cmd = "/bin/echo " .. out2 .. " > /sys/devices/virtual/gpio/gpio497/value" 
		luci.util.exec(cmd)
		return out2
	end
end

function c:validate(value)
	local out3
	--return value:match("[0-1]")
	out3 = value:match("[0-1]")
	
	if out3 == nil then
		return nil
	else
		local cmd = "/bin/echo " .. out3 .. " > /sys/devices/virtual/gpio/gpio498/value" 
		luci.util.exec(cmd)
		return out3
	end
end

function e:validate(value)
	local out4
	--return value:match("[0-1]")
	out4 = value:match("[0-1]")
	
	if out4 == nil then
		return nil
	else
		local cmd = "/bin/echo " .. out4 .. " > /sys/devices/virtual/gpio/gpio499/value" 
		luci.util.exec(cmd)
		return out4
	end
end





-- Dummy option to go back to overview
back = d:option(DummyValue, "_overview", translate("Back to Overview"))
back.value = ""
back.titleref = luci.dispatcher.build_url("admin", "custom", "gpios")


return m




