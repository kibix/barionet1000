local utl = require "luci.util"

m = Map("gpios", translate("GPIO Configuration"), translate("Please select 0 or 1 "))

d = m:section(TypedSection, "relays", "Relay Toggle Section ")  -- info is the section called info in cbi_file

-- Button go back to overview
m.redirect = luci.dispatcher.build_url("admin", "custom", "gpios")


a = d:option(Value, "rl1", "Relay 1"); a.optional=false; a.rmempty = false;
b = d:option(Value, "rl2", "Relay 2"); b.optional=false; b.rmempty = false;



-- Dummy option to go back to overview
back = d:option(DummyValue, "_overview", translate("Back to Overview"))
back.value = ""
back.titleref = luci.dispatcher.build_url("admin", "custom", "gpios")




function a:validate(value)
	local rl1
	--return value:match("[0-1]")
	rl1 = value:match("[0-1]")	
	
	if rl1 == nil then
		return nil
	else
		local cmd = "/bin/echo " .. rl1 .. " > /sys/devices/virtual/gpio/gpio501/value" 
		luci.util.exec(cmd)
		return rl1
	end
end

function b:validate(value)
	local rl2
	--return value:match("[0-1]")
	rl2 = value:match("[0-1]")	
	
	if rl2 == nil then
		return nil
	else
		local cmd = "/bin/echo " .. rl2 .. " > /sys/devices/virtual/gpio/gpio500/value" 
		luci.util.exec(cmd)
		return rl2
	end
end





return m

