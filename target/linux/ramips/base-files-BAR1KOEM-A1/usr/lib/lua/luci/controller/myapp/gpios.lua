module("luci.controller.myapp.gpios", package.seeall)  --notice that gpios is the name of the file gpios.lua

function index()
	
	entry({"gpios", "read", "now"}, call("action_gpios_read"), "Gpios read", 20).dependent=false
	entry({"admin", "custom"}, firstchild(), "Hardware", 70).dependent=false
	entry({"admin", "custom", "gpios"}, template("gpios-module/gpiospage"), "GPIOS Page", 71)	
	entry({"admin", "custom", "gpios", "dinputs"}, cbi("gpios-module/dinputs"), "Digital Inputs Edit", 72)	
	entry({"admin", "custom", "gpios", "relay"}, cbi("gpios-module/relay"), "Relays Edit", 73)	
	entry({"admin", "custom", "gpios", "doutputs"}, cbi("gpios-module/doutputs"), "Digital Outputs Edit", 74)	

end



-- Function to read gpios from controller 


function action_gpios_read()


	-- Show The Input contents
	
	luci.http.prepare_content("text/plain")
	luci.http.write("Digital Input Status\n")


	local my_gpios = luci.sys.exec("/root/scan_inputs.sh")

	--luci.http.write(my_gpios)

	local ngpio = 0
	local i = 1
	local x,a,b =  1
	local l_gpio = {}
	
	local key,value = nil

	-- Find string by breakline: http://lua-users.org/lists/lua-l/2007-07/msg00020.html
	while x < string.len(my_gpios) do
    		a, b = string.find(my_gpios, '.-\n', x);
    		if not a then
      			break;
    		else
        	--Do stuff (you can use string.sub with the indicies a and b)
			Line = string.sub(my_gpios, a, b)
			--luci.http.write("IN" .. i .. " " .. Line)		
			l_gpio[i] = Line	
			i = i + 1

		end
		x = b + 1;
	end

	for key,value in pairs(l_gpio) do

		luci.http.write(" IN" .. key .. " " .. value)		
		
	end

	-- Show Analog Inputs
	luci.http.write("\nAnalog Input Status\n")
	

	local my_adcs = luci.sys.exec("/root/adc_test.sh")
	
	local nadc = 0
	local i = 1
	local x,a,b =  1
	local l_adc = {}
	local key,value = nil	
		
	 while x < string.len(my_adcs) do
    		a, b = string.find(my_adcs, '.-\n', x);
    		if not a then
      			break;
    		else
        	--Do stuff (you can use string.sub with the indicies a and b)
			Line = string.sub(my_adcs, a, b)
			--luci.http.write("AN" .. i .. " " .. Line)		
			l_adc[i] = Line	
			i = i + 1

		end
		x = b + 1;
	end

	for key,value in pairs(l_adc) do

		luci.http.write(" AN" .. key .. " " .. value)		
		
	end



	-- Show Relay outputs
	
	luci.http.write("\nRelay output status\n")
	
	local relay_1, relay_2 = nil
	relay_1 = luci.sys.exec("cat /sys/devices/virtual/gpio/gpio501/value")
	relay_2 = luci.sys.exec("cat /sys/devices/virtual/gpio/gpio500/value")
	
	
	luci.http.write(" Relay 1 " .. relay_1)		
	luci.http.write(" Relay 2 " .. relay_2)


	-- Show Digital Outputs		

	luci.http.write("\nDigital MOSFET output status\n")
	
	local out_1, out_2 = nil
	local out_3, out_4 = nil
	out_1 = luci.sys.exec("cat /sys/devices/virtual/gpio/gpio496/value")
	out_2 = luci.sys.exec("cat /sys/devices/virtual/gpio/gpio497/value")
	out_3 = luci.sys.exec("cat /sys/devices/virtual/gpio/gpio498/value")
	out_4 = luci.sys.exec("cat /sys/devices/virtual/gpio/gpio499/value")
	
	luci.http.write(" Digital Out 1 " .."\t" .. out_1)		
	luci.http.write(" Digital Out 2 " .."\t" .. out_2)		
	luci.http.write(" Digital Out 3 " .."\t" .. out_3)		
	luci.http.write(" Digital Out 4 " .."\t" .. out_4)		



end


