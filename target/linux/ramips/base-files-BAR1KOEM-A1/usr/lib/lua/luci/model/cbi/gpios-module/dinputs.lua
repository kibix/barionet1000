local utl = require "luci.util"

m = Map("gpios", translate("GPIO Configuration"), translate("Please select 0 or 1 "))

d = m:section(TypedSection, "dinputs", "Digital Input Pull up Section ")  -- info is the section called info in cbi_file

-- Button go back to overview
m.redirect = luci.dispatcher.build_url("admin", "custom", "gpios")

a = d:option(Value, "in1", "IN1 Pull UP"); a.optional=false; a.rmempty = false;
b = d:option(Value, "in2", "IN2 Pull UP"); b.optional=false; b.rmempty = false;
c = d:option(Value, "in3", "IN3 Pull UP"); c.optional=false; c.rmempty = false;
e = d:option(Value, "in4", "IN4 Pull UP"); e.optional=false; e.rmempty = false;
f = d:option(Value, "in5", "IN5 Pull UP"); f.optional=false; f.rmempty = false;
g = d:option(Value, "in6", "IN6 Pull UP"); g.optional=false; g.rmempty = false;
h = d:option(Value, "in7", "IN7 Pull UP"); h.optional=false; h.rmempty = false;
i = d:option(Value, "in8", "IN8 Pull UP"); i.optional=false; i.rmempty = false;

-- Dummy option to go back to overview
back = d:option(DummyValue, "_overview", translate("Back to Overview"))
back.value = ""
back.titleref = luci.dispatcher.build_url("admin", "custom", "gpios")


function a:validate(value)
	local in1
	--return value:match("[0-1]")
	in1 = value:match("[0-1]")	
	
	if in1 == nil then
		return nil
	else
		local cmd = "/bin/echo " .. in1 .. " > /sys/class/gpio/gpio488/value" 
		luci.util.exec(cmd)
		return in1
	end
end

function b:validate(value)
	local in2
	--return value:match("[0-1]")
	in2 = value:match("[0-1]")	
	
	if in2 == nil then
		return nil
	else
		local cmd = "/bin/echo " .. in2 .. " > /sys/class/gpio/gpio489/value" 
		luci.util.exec(cmd)
		return in2
	end
end

function c:validate(value)
	local in3
	--return value:match("[0-1]")
	in3 = value:match("[0-1]")	
	
	if in3 == nil then
		return nil
	else
		local cmd = "/bin/echo " .. in3 .. " > /sys/class/gpio/gpio490/value" 
		luci.util.exec(cmd)
		return in3
	end
end

function e:validate(value)
	local in4
	--return value:match("[0-1]")
	in4 = value:match("[0-1]")	
	
	if in4 == nil then
		return nil
	else
		local cmd = "/bin/echo " .. in4 .. " > /sys/class/gpio/gpio491/value" 
		luci.util.exec(cmd)
		return in4
	end
end

function f:validate(value)
	local in5
	--return value:match("[0-1]")
	in5 = value:match("[0-1]")	
	
	if in5 == nil then
	
	return nil
	else
		local cmd = "/bin/echo " .. in5 .. " > /sys/class/gpio/gpio492/value" 
		luci.util.exec(cmd)
		return in5
	end
end

function g:validate(value)
	local in6
	--return value:match("[0-1]")
	in6 = value:match("[0-1]")	
	
	if in6 == nil then
	
	return nil
	else
		local cmd = "/bin/echo " .. in6 .. " > /sys/class/gpio/gpio493/value" 
		luci.util.exec(cmd)
		return in6
	end
end

function h:validate(value)
	local in7
	--return value:match("[0-1]")
	in7 = value:match("[0-1]")	
	
	if in7 == nil then
	
	return nil
	else
		local cmd = "/bin/echo " .. in7 .. " > /sys/class/gpio/gpio494/value" 
		luci.util.exec(cmd)
		return in7
	end
end

function i:validate(value)
	local in8
	--return value:match("[0-1]")
	in8 = value:match("[0-1]")	
	
	if in8 == nil then
	
	return nil
	else
		local cmd = "/bin/echo " .. in8 .. " > /sys/class/gpio/gpio495/value" 
		luci.util.exec(cmd)
		return in8
	end
end





return m
