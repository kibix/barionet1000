# About #

**openwrt** repository holds all the customized images of OpenWRT operating system.

# Build #

Clone repository to the local workstation:

```
git clone https://<username>@bitbucket.org/kibix/barionet1000.git ./openwrt
git fetch
git pull
git fetch -t
```

Follow standard OpenWRT build procedure:

```
cd ./openwrt
scripts/feeds update -a
scripts/feeds install -a
make menuconfig
# ... check proper target and profile, e.g. BAR1KOEM-A1
# ... save changes
make defconfig
```

***IMPORTANT: Apply kernel configuration patch*** (or select appropriate hardware drivers and kernel settings using ``make kernel_menuconfig`` command):
```
git apply target/linux/ramips/mt7620/config-4.4.bar1000-a4.patch
```

Proceed with build
```
make -j4
```

See also: [OpenWrt build system](https://wiki.openwrt.org/doc/howto/buildroot.exigence)

# Adding new changes #

Changes to **master** branch accepted through ***pull requests only!***

```
git fetch
git checkout master
git pull
git checkout <new branch name>

# ... do your changes
git commit -m "Added/Changed ... describe your changes/fix"

# ... build, test

git fetch origin
git rebase remotes/origin/master

# ... build, test, test, test, test...
# ... create a new poll request in Bitbucket
```

# Fetch Latest OpenWRT Changes #

Once in a while it is useful to integrate all the latest OpenWRT changes. 

***You should know what you are doing before you proceed!***

Create remote OpenWRT repo link (only the first time):

```
cd ./openwrt
git remote add openwrt https://github.com/openwrt/openwrt.git
```

Download latest OpenWRT changes and ***replay local changes on top***.

```
git fetch openwrt
git rebase remotes/openwrt/master
git push
```

# Support #

Send your questions to support@barix.com.